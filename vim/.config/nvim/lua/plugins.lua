local basic_lsps = {
  "gopls",
  "lua_ls",
  "terraformls",
}

local ts_languages = {
  "bash",
  "css",
  "dockerfile",
  "git_config",
  "gitcommit",
  "go",
  "gomod",
  "html",
  "ini",
  "json",
  "latex",
  "lua",
  "make",
  "markdown",
  "markdown_inline",
  "python",
  "rasi",
  "regex",
  "ruby",
  "rust",
  "scss",
  "sql",
  "terraform",
  "toml",
  "vim",
  "vimdoc",
  "xml",
  "yaml",
}

return {
  {
    "dracula/vim",
    lazy = false,
    priority = 1000,
    config = function()
      vim.cmd.colorscheme("dracula")
    end,
  },
  {
    "nvim-tree/nvim-web-devicons",
  },
  {
    "nvim-lua/plenary.nvim",
  },
  {
    "nvim-tree/nvim-tree.lua",
    version = "*",
    lazy = false,
    config = function()
      require("nvim-tree").setup({})
    end,
  },
  {
    "lewis6991/gitsigns.nvim",
    config = function()
      require("gitsigns").setup({})
    end,
  },
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function()
      local configs = require("nvim-treesitter.configs")

      configs.setup({
        ensure_installed = ts_languages,
        sync_install = false,
        highlight = { enable = true },
        indent = { enable = true },
      })
    end
  },
  {
    "nvim-telescope/telescope.nvim",
    branch = "0.1.x",
    config = function()
      local telescope = require("telescope")
      local telescopeConfig = require("telescope.config")

      -- clone default configuration
      local vimgrep_arguments = { unpack(telescopeConfig.values.vimgrep_arguments) }

      -- search in hidden/dot files...
      table.insert(vimgrep_arguments, "--hidden")
      -- ...but not in the `.git` directory
      table.insert(vimgrep_arguments, "--glob")
      table.insert(vimgrep_arguments, "!**/.git/*")

      telescope.setup({
        defaults = {
          vimgrep_arguments = vimgrep_arguments,
        },
        pickers = {
          find_files = {
            find_command = { "rg", "--files", "--follow", "--hidden", "--glob", "!**/.git/*" },
          },
        },
      })
    end,
  },
  {
    "nvim-telescope/telescope-fzf-native.nvim",
    build = "make",
    config = function()
      require("telescope").load_extension("fzf")
    end,
  },
  {
    "debugloop/telescope-undo.nvim",
    config = function()
      require("telescope").load_extension("undo")
    end,
  },
  {
    "numToStr/Comment.nvim",
    opts = {},
    lazy = false,
  },
  {
    "kylechui/nvim-surround",
    version = "*",
    event = "VeryLazy",
    config = function()
      require("nvim-surround").setup({})
    end,
  },
  {
    "ms-jpq/coq_nvim",
    branch = "coq",
    config = function()
      vim.cmd("COQnow -s")
    end,
  },
  {
    "ms-jpq/coq.artifacts",
    branch = "artifacts",
  },
  {
    "williamboman/mason.nvim",
    config = function()
      require("mason").setup({})
    end,
  },
  {
    "williamboman/mason-lspconfig.nvim",
    config = function()
      require("mason-lspconfig").setup({
        ensure_installed = basic_lsps,
      })
    end,
  },
  {
    "neovim/nvim-lspconfig",
    config = function()
      local lspconfig = require("lspconfig")
      local coq = require("coq")

      for _, val in pairs(basic_lsps) do
        lspconfig[val].setup(coq.lsp_ensure_capabilities({}))
      end
    end,
  },
  {
    "sindrets/diffview.nvim",
  },
  {
    "NeogitOrg/neogit",
    config = true,
  },
}
