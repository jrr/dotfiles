HOME = os.getenv("HOME")

-- Plugin manager bootstrap

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- General

vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

vim.opt.termguicolors = true

vim.opt.encoding = "utf-8"

vim.opt.autoindent = true

vim.opt.backup = false
vim.opt.swapfile = false
vim.opt.undofile = true

vim.opt.scrolloff = 3

vim.opt.wrap = false
vim.opt.wrapmargin = 10

vim.opt.showcmd = true
vim.opt.lazyredraw = true

-- Tabs

vim.opt.expandtab = true
vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2
vim.opt.tabstop = 2

-- Listchars

vim.opt.list = true
vim.opt.listchars = {
  tab = "→ ",
  extends = "»",
  precedes = "«",
  nbsp = "·",
  trail = "·"
}

-- Direction finding

vim.opt.number = true
vim.opt.ruler = true

-- Search

vim.opt.hlsearch = true
vim.opt.ignorecase = true
vim.opt.incsearch = true
vim.opt.showmatch = true
vim.opt.smartcase = true

-- Syntax/Colour

vim.opt.modeline = true

-- Statusline

vim.opt.laststatus = 2

-- Autocommands

vim.api.nvim_create_autocmd("BufWritePre", {
  pattern = "*.go",
  callback = function() vim.lsp.buf.format() end,
})

vim.api.nvim_create_autocmd({"BufWritePre"}, {
  pattern = {"*.tf", "*.tfvars"},
  callback = function()
    vim.lsp.buf.format()
  end,
})

-- Mapping

vim.g.mapleader = ","
vim.opt.backspace = { "indent", "eol", "start" }

function map_opts(desc)
  return { desc = desc, noremap = true, silent = true, nowait = true }
end

function remap(mode, shortcut, command, desc)
  vim.keymap.set(mode, shortcut, command, map_opts(desc))
end

function nmap(shortcut, command, desc)
  remap("n", shortcut, command, desc)
end

function vmap(shortcut, command, desc)
  remap("v", shortcut, command, desc)
end

nmap("<leader>bb", "<cmd>buffers<CR>", "Show buffers")
nmap("<leader>bn", "<cmd>bn<CR>", "Next buffer")
nmap("<leader>bp", "<cmd>bp<CR>", "Previous buffer")

nmap("<leader>h", "<cmd>nohlsearch<CR>", "Clear search highlight")
nmap("<leader>W", "<cmd>%s/\\s\\+$//g<CR>", "Delete trailing whitespace")

-- System clipboard copy/paste

vmap("<leader>c", "\"+y<CR>", "Yank into system clipboard")
nmap("<leader>v", "\"+gP", "Paste from system clipboard")

-- Set up plugins

require("lazy").setup("plugins")

-- Plugin mappings

nmap("<leader>t", "<cmd>NvimTreeToggle<CR>", "Toggle NvimTree")

nmap("<leader>u", "<cmd>Telescope undo<CR>", "Undo tree")

nmap("<leader>fb", "<cmd>Telescope buffers<CR>", "Buffer explorer")
nmap("<leader>ff", "<cmd>Telescope find_files<CR>", "File finder")
nmap("<leader>fg", "<cmd>Telescope live_grep<CR>", "Grep")
