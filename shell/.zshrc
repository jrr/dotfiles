source ~/.zsh_aliases

fpath+=~/.zfunc

autoload -U compinit && compinit
autoload -U bashcompinit && bashcompinit

# kill global prompt options
#prompt off

# some readline niceties
bindkey "\e[1~" beginning-of-line
bindkey "\e[4~" end-of-line
bindkey "\e[5~" beginning-of-history
bindkey "\e[6~" end-of-history
bindkey "\e[7~" beginning-of-line
bindkey "\e[3~" delete-char
bindkey "\e[2~" quoted-insert
bindkey "\e[5C" forward-word
bindkey "\e[5D" backward-word
bindkey "\e\e[C" forward-word
bindkey "\e\e[D" backward-word
bindkey "\e[1;5C" forward-word
bindkey "\e[1;5D" backward-word

setopt prompt_subst
export PROMPT='%F{green}%n%f@%F{magenta}%m%f > '

# Shell history
export HISTFILE=~/.zsh_history
export SAVEHIST=1000000
export HISTSIZE=1000000
setopt extended_history
setopt inc_append_history_time
setopt hist_no_functions
setopt hist_no_store
setopt hist_reduce_blanks
setopt hist_ignore_dups
setopt hist_save_no_dups
setopt no_hist_beep

# EDITOR
if [[ $(which nvim) ]]; then
  export EDITOR=$(which nvim)
else
  export EDITOR=$(which vim)
fi

# PATH
export PATH="$HOME/bin:$HOME/.local/bin:$PATH"

# Miscellaneous variables
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_DATA_DIRS="$XDG_DATA_HOME:/usr/local/share:/usr/share"

# ssh-add
ssh-add 2> /dev/null

function __exists() {
  builtin type $1 >/dev/null 2>&1
}

# dircolors
if [[ $(uname) = 'Darwin' ]]; then
  [[ -e ~/.lscolors ]] && eval $(gdircolors ~/.lscolors)
else
  [[ -e ~/.lscolors ]] && eval $(dircolors ~/.lscolors)
fi

# golang
if [[ -d /usr/lib/go ]]; then
  export GOPATH="$HOME/go"
  export GOROOT="/usr/lib/go"
  export PATH="$PATH:${GOROOT}/bin:$GOPATH/bin"
fi

# kubernetes
if __exists kubectl; then
  if [[ ! -f ${HOME}/.zfunc/kubectl.zsh ]]; then
    kubectl completion zsh > ${HOME}/.zfunc/kubectl.zsh
    chmod +x ${HOME}/.zfunc/kubectl.zsh
  fi

  source ${HOME}/.zfunc/kubectl.zsh

  if [[ -d ${HOME}/.krew/bin ]]; then
    export PATH="${PATH}:${HOME}/.krew/bin"
  fi
fi

# nvm
if [ -d ${HOME}/.nvm ]; then
  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"
fi

# racer
if [[ -d ${HOME}/.racer ]]; then
  export RUST_SRC_PATH="$HOME/.racer/rustsrc"
fi

# rbenv
if [ -d ${HOME}/.rbenv ]; then
  export PATH="$HOME/.rbenv/bin:$PATH"
  eval "$(rbenv init -)"
fi

# rust
if __exists rustc; then
  export PATH="$HOME/.cargo/bin:$PATH"
fi

# rustup
if [[ -d ${HOME}/.cargo ]] && [[ -f ${HOME}/.cargo/env ]]; then
  source $HOME/.cargo/env
fi

# fzf
if __exists fzf; then
  source /usr/share/fzf/key-bindings.zsh
  source /usr/share/fzf/completion.zsh
fi

# starship.rs
if __exists starship; then
  eval "$(starship init zsh)"
fi

# GCP CLI
if [[ -d ${HOME}/google-cloud-sdk ]]; then
  export USE_GKE_GCLOUD_AUTH_PLUGIN=True
  source ${HOME}/google-cloud-sdk/path.zsh.inc
  source ${HOME}/google-cloud-sdk/completion.zsh.inc
fi
